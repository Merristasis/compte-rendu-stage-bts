# Journal quotidien - Stage BTS (Syrlinks SAS)

## Jour 1 
Tour de l'entreprise

## Jour 2 
Rédaction du cahier des charges et demande de la part des maîtres de stage de réfléchir à plusieurs solutions (en + des pistes déjà données)

## Jour 3
Rédaction du cahier des charges, pistes trouvées

## Jour 4 
Appels vers les entreprises 

## Jour 5 
Solution fixée (Keyence)

## Jour 6
Recherches

## Jour 7 
Autre solution trouvée (Laser panasonic)

## Jour 8
Cahier des charges

## Jour 9
Commande des produits

